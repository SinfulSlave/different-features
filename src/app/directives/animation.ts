/* tslint:disable */
import { style, animate, keyframes } from '@angular/animations';

/**********  Entries Animations ********************/

export const XpAnimations: any = {}

XpAnimations['scale_in_top'] = animate('0.5s 0.1s cubic-bezier(0.250, 0.460, 0.450, 0.940)', keyframes([
  style({ visibility: 'visible', opacity: 0, }),
  style({ transform: 'scale(0)', transformOrigin: '50% 0%', }),
  style({ transform: 'scale(1)', transformOrigin: '50% 0%', opacity: 1, }),
]));

XpAnimations['scale_in_hor_center'] = animate('0.5s 0.1s cubic-bezier(0.250, 0.460, 0.450, 0.940)', keyframes([
  style({ visibility: 'visible', opacity: 0, }),
  style({ transform: 'scaleX(0)', }),
  style({ transform: 'scaleX(1)', opacity: 1, }),
]));