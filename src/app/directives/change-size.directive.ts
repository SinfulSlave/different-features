import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appChangeSize]',
  standalone: true,
})
export class ChangeSizeDirective {

  @Input() colorBg: string = ''

  @HostBinding('style.background') backgroung = this.colorBg

  constructor() {
   }

   ngOnInit(){
    this.backgroung = this.colorBg
   }
}
