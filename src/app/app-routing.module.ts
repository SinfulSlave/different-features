import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkWithCssConceptionsComponent } from './features/work-with-css-conceptions/work-with-css-conceptions.component';

const routes: Routes = [
  {
    path: 'intersection-observer',
    loadChildren: () => import('./features/intersection-observer/intersection-observer.module').then(m => m.IntersectionObserverModule),
  }
  , {
    path: 'work-with-css',
    component: WorkWithCssConceptionsComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
