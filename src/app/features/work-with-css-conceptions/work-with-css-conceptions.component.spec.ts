import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkWithCssConceptionsComponent } from './work-with-css-conceptions.component';

describe('WorkWithCssConceptionsComponent', () => {
  let component: WorkWithCssConceptionsComponent;
  let fixture: ComponentFixture<WorkWithCssConceptionsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WorkWithCssConceptionsComponent]
    });
    fixture = TestBed.createComponent(WorkWithCssConceptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
