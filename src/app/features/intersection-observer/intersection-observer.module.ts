import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntersectionObserverComponent } from './intersection-observer.component';
import { RouterModule } from '@angular/router';
import { ChangeSizeDirective } from 'src/app/directives/change-size.directive';
import { AppearanceAnimationDirective } from 'src/app/directives/appearance-animation.directive';

@NgModule({
  declarations: [
    IntersectionObserverComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{path: '', component: IntersectionObserverComponent}]),
    ChangeSizeDirective,
    AppearanceAnimationDirective,

  ],
  providers: [
  ]
})
export class IntersectionObserverModule { }
